## Temba

An **Ionic 1.0/Cordova** messenger app written in AngularJS with a **Firebase** backend and giphy integrations.

Based on Paul Cham's **Fireline** [project](https://market.ionicframework.com/user/476608).