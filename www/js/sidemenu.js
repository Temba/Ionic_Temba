// sidemenu.js
// This is the controller that handles the sidemenu abstract state
'Use Strict';
angular.module('TembaFire').controller('appController', function($scope, $rootScope, $state, $stateParams, $ionicSideMenuDelegate, $ionicHistory, $location, $localStorage, Watchers, Service, customModal, multiViewModal ){

    // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
 // });

   //menuActiveController from ionic_sunset://
  $scope.isActive = function(route) {
    return route === $location.path();
  };

    //Logout the user. Clears the localStorage as well as reinitializing the variable of watcherAttached to only trigger attaching of watcher once.
  $scope.logout = function() {
    if (firebase.auth()) {
      firebase.database().ref('accounts/' + $localStorage.accountId).update({
        online: false
      });
      firebase.auth().signOut().then(function() {
        Watchers.removeWatchers();
        //Clear the saved credentials.
        $localStorage.$reset();
        //Proceed to login screen.
        $scope.canChangeView = true;
        $state.go('login');
      }, function(error) {
        //Show error message.
        Utils.message(Popup.errorIcon, Popup.errorLogout);
      });
    } else {
      //Clear the saved credentials.
      $localStorage.$reset();
      //Proceed to login screen.
      $scope.canChangeView = true;
      $state.go('login');
    }
  };

    //TO UPDATE SIDEMENU BADGES//

     //Notify whenever there are new messages.
    $scope.$watch(function() {
      return Service.getUnreadMessages();
    }, function(unreadMessages) {
      $scope.unreadMessages = unreadMessages;
    });

    //Notify whenever there are new friend requests.
    $scope.$watch(function() {
      return Service.getFriendRequestsCount();
    }, function(friendRequests) {
      $scope.friendRequestsCount = friendRequests;
    });

    //Notify whenever there are new group messages.
    $scope.$watch(function() {
      return Service.getUnreadGroupMessages();
    }, function(unreadGroupMessages) {
      console.log('ug' + $scope.unreadGroupMessages);
      $scope.unreadGroupMessages = unreadGroupMessages;
    });

    //Notify whenever there are new chat messages:
   $scope.$watch(function() {
      return Service.getUnreadChatmessages();
    }, function(unreadChatmessages) {
        console.log('uch' + $scope.unreadChatmessages);
      $scope.unreadChatmessages = unreadChatmessages;
    });

    //Disable canChangeView to disable automatically restating to other route whenever Firebase Watcher calls are triggered.
    $scope.canChangeView = false;
    //var unreadGroupMessages=4;

    //Update UserBio and website//
    $scope.$watch(function() {
       return Service.getEditProfileData();
    }, function(editProfileData) {
      $scope.editProfileData = Service.getEditProfileData();
      $scope.editProfileData.userBio =  Service.getEditProfileData().userBio;
      console.log('sidemenu bio ' + $scope.editProfileData.userBio);
    });

    //Update profile whenever there are changes done on the profile.
    $scope.$watch(function() {
      return Service.getProfile();
    }, function(profile) {
      if($scope.changedProfilePic)
        Utils.show();
      $scope.profile = Service.getProfile();
      console.log('sidemenu name ' + $scope.profile.username);
    });

// handle startChat modals//
  /* var multiPageModal = multiViewModal.get('modal1');

  $scope.showStartChat = function(){
    multiPageModal.show();
  };

  $scope.closeStartChat = function () {
    multiPageModal.close();
  };

  $scope.activateMenu = function (name) {
    multiPageModal.activateView(name);
      console.log (name);
  };

  $scope.previous = function () {
    multiPageModal.previousView();
  };

    // Modal service initialization.
  var simpleModal = customModal.get('modal2');

  $scope.showInfo2 = function () {
    simpleModal.show();
  };

  $scope.closeInfo2 = function () {
    simpleModal.close();
  }

  //Hashtag list
  $scope.tags = []; */


});

