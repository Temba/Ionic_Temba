// chatDetails.js
// This is the controller that handles the chatDetails view of the selected chat.
// The user can add participants or leave/delete the chat in this view.
'Use Strict';
angular.module('TembaFire').controller('chatDetailsController', function($scope, $state, $localStorage, Popup, Utils, $filter, $ionicScrollDelegate, $ionicHistory, Service, $timeout, $cordovaCamera) {
  //Prevent automatically restating to messages route when Firebase Watcher calls are triggered.
  $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
    if (!$scope.canChangeView) {
      event.preventDefault();
    }
  });

  //Allow going back when back is selected.
  $scope.back = function() {
    $scope.canChangeView = true;
    //Clear assignedIds, assignedIds filters the friends to be shown on filter when they are already assigned on a chat.
    Service.clearAssignedIds();
    $state.go('app.chat');
  };

  $scope.$on('$ionicView.enter', function() {
    //Disable canChangeView to disable automatically restating to messages route whenever Firebase Watcher calls are triggered.
    $scope.canChangeView = false;
    //Retrieve chat using the Service.
    $scope.chat = Service.getChatById($localStorage.chatId);
    $scope.mode = 'Chat';

    //$scope.chatTags = [];
    $scope.chatTags = Service.getChatById($localStorage.chatId).chatTags;
      console.log($scope.chat);
      console.log('tags ' + $scope.chatTags);

    for (var i = 0; i < $scope.chat.users.length; i++) {
      $scope.chat.users[i].profilePic = Service.getProfilePic($scope.chat.users[i].id);
    }

    //check if local user is the chat creator:
  $scope.chatCreator= Service.getChatById($localStorage.chatId).chatCreator;
    console.log($scope.chatCreator);
    console.log($localStorage.accountId);
      if ($scope.chatCreator===$localStorage.accountId)
          $scope.isChatCreator=true;
      else
          $scope.isChatCreator=false;
          //$scope.isnotChatCreator=true;
           console.log($scope.isChatCreator);
          // console.log($scope.isnotChatCreator);

    //Set friendList when we are adding participants to the chat.
    $scope.friends = [];
    $scope.friends = Service.getFriendList();


    //Add chat participants to the assignedIds, assignedIds filters the friends to be shown on filter when they are already assigned on a chat.
    for (var i = 0; i < $scope.chat.users.length; i++) {
      Service.addAssignedIds($scope.chat.users[i].id);
    }
  });

  //Broadcast when new chat image is uploaded, delete the previous chat image.
  $scope.$on('imageUploaded', function(event, args) {
    //Delete the previous chat image, and set to the new chat image.
    firebase.database().ref('chats/' + $localStorage.chatId).once('value', function(chat) {
      if(chat.val().image != 'img/chat.png')
        firebase.storage().refFromURL(chat.val().image).delete();
    });
    firebase.database().ref('chats/' + $localStorage.chatId).update({
      image: args.imageUrl
    });
    $scope.chat.image = args.imageUrl;
  });

  //Confirm where the new chat picture will come from, gallery or camera?
  $scope.changeChatPic = function() {
    var popup = Utils.confirm('ion-link', 'chat Picture: Do you want to take a photo or choose from your gallery?', 'ion-images', 'ion-camera');
    popup.then(function(isCamera) {
      var imageSource;
      if (isCamera) {
        imageSource = Camera.PictureSourceType.CAMERA;
      } else {
        imageSource = Camera.PictureSourceType.PHOTOLIBRARY;
      }
      //Show loading.
      Utils.getProfilePicture(imageSource);
    });
  };

  //Change mode to adding participants to the chat.
  $scope.addParticipant = function() {
    $scope.mode = 'Add';
  };

  //Change mode to viewing details of the chat.
  $scope.cancelAddParticipant = function() {
    $scope.mode = 'Chat';
  };

  //Add selected friend to the chat, create Firebase data to accomodate the changes.
  $scope.addToChat = function(friend) {
    firebase.database().ref('chats/' + $localStorage.chatId).once('value', function(chat) {
      var chat = chat.val();
      var users = chat.users;
      var chatmessages = chat.chatmessages;
      if (!users) {
        users = [];
      }
      if (!chatmessages) {
        chatmessages = [];
      }
      users.push(friend.id);
      //Add a message to the chat saying that you added a participant to the chat.
      chatmessages.push({
        sender: $localStorage.accountId,
        chatmessage: "has added " + friend.name + " to the chat.",
        date: Date(month, day, hours, minutes),
        type: 'text'
      });
      firebase.database().ref('chats/' + $localStorage.chatId).update({
        users: users,
        chatmessages: chatmessages
      });
      firebase.database().ref('accounts/' + friend.id).once('value', function(account) {
        var chats = account.val().chats;
        if (!chats) {
          chats = [];
        }
        chats.push({
          chat: $localStorage.chatId,
          chatmessagesRead: 0
        });
        firebase.database().ref('accounts/' + friend.id).update({
          chats: chats
        });
      });

      //Add to assignedIds so recently added participant won't show up on the add participant list.
      Service.addAssignedIds(friend.id);
    });
  };

  //Leave chat - will delete Firebase data to accomodate leaving the chatstream.
  $scope.leaveChat = function() {
    var popup = Utils.confirm('ion-chatboxes', 'Are you sure you want to leave this chat?', 'ion-close', 'ion-checkmark');
    popup.then(function(proceed) {
      if (proceed) {
        firebase.database().ref('chats/' + $localStorage.chatId).once('value', function(chat) {
          var chat = chat.val();
          var users = chat.users;
          var chatmessages = chat.chatmessages;
          if (!chatmessages) {
            chatmessages = [];
          }
          users.splice(users.indexOf($localStorage.accountId), 1);
          firebase.database().ref('accounts/' + $localStorage.accountId).once('value', function(account) {
            var name = account.val().name;
            //Add a message to the chatstream saying that you left the chat.
            chatmessages.push({
              sender: $localStorage.accountId,
              chatmessage: name + " has left this chat.",
              date: Date(month, day, hours, minutes),
              type: 'text'
            });
            firebase.database().ref('chats/' + $localStorage.chatId).update({
              users: users,
              chatmessages: chatmessages
            });

            var chats = account.val().chats;
            var index = -1;
            for (var i = 0; i < chats.length; i++) {
              if (chats[i].chat == $localStorage.chatId) {
                index = i;
              }
            }
            if (index > -1) {
              chats.splice(index, 1);
            }
            firebase.database().ref('accounts/' + $localStorage.accountId).update({
              chats: chats
            });
          });
          //Clear assignedIds and proceed to chats selection.
          Service.clearAssignedIds();
          $scope.canChangeView = true;
          $state.go('app.chats');
        });
      }
    });
  };

// CHECK OUT THIS FUNCTION - DON'T WANT TO SIMPLY DELETE, BUT RATHER DELETE OR SAVE, AND LEAVE IT UP TO THE HOST !!!!

  //Deleting a chatstream, when only one participant was left to the chat.
  //Delete chat on Firebase.
  $scope.deletechat = function() {
    var popup = Utils.confirm('ion-chatboxes', 'Are you sure you want to delete this chat?', 'ion-close', 'ion-checkmark');
    popup.then(function(proceed) {
      if (proceed) {
        firebase.database().ref('accounts/' + $localStorage.accountId).once('value', function(account) {
          var chats = account.val().chats;
          var index = -1;
          for (var i = 0; i < chats.length; i++) {
            if (chats[i].chat == $localStorage.chatId) {
              index = i;
            }
          }
          if (index > -1) {
            chats.splice(index, 1);
          }
          firebase.database().ref('accounts/' + $localStorage.accountId).update({
            chats: chats
          });
        });
        //Delete chatImage.
        firebase.database().ref('chats/' + $localStorage.chatId).once('value', function(chat) {
          if(chat.val().image != 'img/chat.png')
            firebase.storage().refFromURL(chat.val().image).delete();
        });
        firebase.database().ref('chats/' + $localStorage.chatId).remove();
        //Clear assignedIds and proceed to chats selection.
        Service.clearAssignedIds();
        $scope.canChangeView = true;
        $state.go('app.chats');
      }
    });
  };
});
