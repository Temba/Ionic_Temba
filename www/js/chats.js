// chats.js
// This is the controller that handles the chats of the user.
// Selecting a chat will open the chatstream.
'Use Strict';
angular.module('TembaFire').controller('chatsController', function($scope, $state, $stateParams, $localStorage, Utils, Popup, $timeout, Service, $ionicTabsDelegate, $ionicHistory) {

//THIS IS A BIG CHANGE  - moved destruction of chat images for cancelled chats to statechangestart rather than on changetab click //

   $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {

       $scope.mode = 'Chats';
    //If user uploaded a picture but didn't proceed with the chat creation, delete the image.
    if($scope.chat && !$scope.chatCreated) {
      if ($scope.chat.image != 'img/chat.png') {
        firebase.storage().refFromURL($scope.chat.image).delete();
      }
    }
    //Clear assignedIds, assignedIds filters the friends to be shown on filter when they are already assigned on a chat.
    Service.clearAssignedIds();
    $scope.canChangeView = true;
    //$state.go(stateTo);// not needed b/c now called on statechangestart(?)//
  });

//The following is as normal from Firebase Messenger

  $scope.$on('$ionicView.enter', function() {
    $scope.mode = 'Chats';
    //Initialize our models from the Service.
    $scope.friends = [];
    $scope.friends = Service.getFriendList();
      console.log('friends '+ $scope.friends.length);
    $scope.chats = [];
    $scope.chats = Service.getChatList();
        console.log('getchatlist  ' +    $scope.chats);
     console.log('chats ' + $scope.chats.length + typeof($scope.chats));
      $scope.profilePic = Service.getProfilePic($localStorage.accountId);
      console.log('profilepic', $scope.profilePic);
    //Notify whenever there are new messages.
    $scope.$watch(function() {
      return Service.getUnreadMessages();
    }, function(unreadMessages) {
      $scope.unreadMessages = unreadMessages;
        console.log('unread messages ' + $scope.unreadMessages);
    });
    //Notify whenever there are new friend requests.
    $scope.$watch(function() {
      return Service.getFriendRequestsCount();
    }, function(friendRequests) {
      $scope.friendRequestsCount = friendRequests;
    });
    //Notify whenever there are new chat messages.
    $scope.$watch(function() {
      return Service.getUnreadChatmessages();
    }, function(unreadChatmessages) {
      $scope.unreadChatmessages = unreadChatmessages;
        console.log('unread_chatmessages  ' + $scope.unreadChatmessages + typeof( $scope.unreadChatmessages));
    });
     //Notify whenever there are new chat invites:
     $scope.$watch(function() {
      return Service.getChatInvitesCount();
    }, function(chatInvites) {
      $scope.chatInvitesCount = chatInvites;
         console.log('chatinvites ' + $scope.chatInvites);
    });
    //Disable canChangeView to disable automatically restarting to messages route whenever Firebase Watcher calls are triggered.
    $scope.canChangeView = false;
    //Indicator if user created a chat or not.
    $scope.chatCreated = false;
    //Select the 2nd tab on the footer to highlight the chats icon.
    //$ionicTabsDelegate.select(1);
  });
  //Set chat image while deleting the previous uploaded image.
  $scope.$on('imageUploaded', function(event, args) {
    if ($scope.chat.image != 'img/chat.png') {
      firebase.storage().refFromURL($scope.chat.image).delete();
    }
    $scope.chat.image = args.imageUrl;
  });
  //Function to assign a participant to the chat, this doesn't insert to the database yet.
  $scope.addToChat = function(friend) {
    $scope.chat.members.push(friend);
    Service.addAssignedIds(friend.id);
  };



  //Function to create chat, creates Firebase database data.
  $scope.createChat = function() {
    var users = [$localStorage.accountId];
    angular.forEach($scope.chat.members, function(member) {
      users.push(member.id);
    });
    var chatmessages = [];
    //Sends a welcome message to everyone to the chat.
    chatmessages.push({
      sender: $localStorage.accountId,
      chatmessage: 'Welcome to ' + $scope.chat.name,
      date: Date(),
      type: 'text'
    });
    var chatId = firebase.database().ref('chats').push({
      name: $scope.chat.name,
      image: $scope.chat.image,
      users: users,
      chatmessages: chatmessages,
      dateCreated: Date(),
     chatCreator:  $localStorage.accountId,
     chatCreatorProfilePic: $scope.profilePic,
      public: $scope.public,
      discoverable:  $scope.discoverable,
      chatTags: $scope.chatTags,
     chatDescription: $scope.chat.chatDescription

    }).key;

    firebase.database().ref('accounts/' + $localStorage.accountId).once('value', function(account) {
      var chats = account.val().chats;
      if (!chats) {
        chats = [];
      }
    var chatsCreated = account.val().chatsCreated;
      if (!chatsCreated) {
        chatsCreated = [];
      }
      chats.push({
        chat: chatId,
        chatmessagesRead: 1
      });
        chatsCreated.push({
        chat: chatId
      });
      firebase.database().ref('accounts/' + $localStorage.accountId).update({
        chats: chats,
        chatsCreated: chatsCreated
      });
    });

    angular.forEach($scope.chat.members, function(member) {
      firebase.database().ref('accounts/' + member.id).once('value', function(account) {
        var chats = account.val().chats;
        if (!chats) {
          chats = [];
        }
        chats.push({
          chat: chatId,
          chatmessagesRead: 0
        });
        firebase.database().ref('accounts/' + member.id).update({
          chats: chats
        });
      });
    });


    //Set our variables then proceed to open the chat so the user can chat directly.
    $scope.chatCreated = true;
    Service.clearAssignedIds();
    $localStorage.chatId = chatId;
    $scope.canChangeView = true;
    $state.go('app.chat');
  };

  //Set mode to compose a chat, while clearing fields.
  $scope.compose = function() {
    $scope.mode = 'Compose';
    $scope.chat = {
      name: "",
      image: 'img/chat.png',
      members: [],

      chatDescription: ""


    };
Service.clearAssignedIds();
  };

//COMPOSE CHAT SCREEN
 //Hashtag list:
  $scope.chatTags = [];
  //$scope.chatDescription='';

//toggles:
$scope.public= false;

    $scope.toggleChange1 = function() {
        if ($scope.public == false) {
            $scope.public = true;
        } else
            $scope.public = false;
            console.log('public changed to ' + $scope.public);
            };

$scope.discoverable= false;

    $scope.toggleChange2 = function() {
        if ($scope.discoverable== false) {
            $scope.discoverable = true;
        } else
            $scope.chat.discoverable = false;
            console.log('discoverable changed to ' + $scope.discoverable);
            };


  //Set mode to view the chats list. If user uploaded an image but didn't proceed with chat creation, delete the image.
  $scope.cancel = function() {
    $scope.mode = 'Chats';
    if($scope.chat && !$scope.chatCreated) {
      if ($scope.chat.image != 'img/chat.png') {
        firebase.storage().refFromURL($scope.chat.image).delete();
      }
    }
    Service.clearAssignedIds();
  };

  //Function to assign a chat picture to the chat to create, calls imageUploaded function on top when Firebase is done uploading the image.
  $scope.changechatPic = function() {
    var popup = Utils.confirm('ion-link', 'chat Picture: Do you want to take a photo or choose from your gallery?', 'ion-images', 'ion-camera');
    popup.then(function(isCamera) {
      var imageSource;
      if (isCamera) {
        imageSource = Camera.PictureSourceType.CAMERA;
      } else {
        imageSource = Camera.PictureSourceType.PHOTOLIBRARY;
      }
      Utils.getProfilePicture(imageSource);
    });
  };

  //Constrains our selected picture to be of same width and height, to preserve proportion:
  $scope.constrainProportion = function() {
    var img = document.getElementById('chatPic');
    var width = img.width;
    img.style.height = width + "px";
  };

  //Remove assignedMember to a chat. Doesn't remove Firebase data just yet:
  $scope.remove = function(index) {
    Service.removeFromAssignedIds($scope.chat.members[index].id);
    $scope.chat.members.splice(index, 1);
  };

  //Proceed to chat in the selected chatstream:
  $scope.enterChat= function(chat) {
    $localStorage.chatId = chat.id;
      console.log('chatId '+ chat.id);
    $scope.canChangeView = true;
    $state.go('app.chat');
  };


   //make ProfilePics clickable to lead to profilepage:

    $scope.goToProfile = function(profile) {
       //$scope.chats=

         //for (var i = 0; i < $scope.chatmessages.length; i++) {
             $scope.chats[i].profile = Service.getProfile($scope.chats[i].chatCreator);
             //$scope.userID[i]=Service.getProfile($scope.chats[i].chatCreator);
        //$localStorage.userId = userId;
        console.log('profile '+ $scope.chats[i].profile.username);
        $scope.canChangeView = true;
        $state.go('app.userProfile', {userID: $scope.chats[i].profile});
     };

});
