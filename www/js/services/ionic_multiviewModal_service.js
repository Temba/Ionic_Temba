angular.module('multiViewModal.services', [])
/**
 * Helps to access and manipulate navigatable modal instances with
 * multiple inner views.

Modified from  Evgenij Olshanskij 2016
 */
.factory('multiViewModal', ['customModal', function (customModal) {

  // container to hold all available modal instances
  var modals = [];

  // the methods defined bellow partially replicate the methods of 'custom modal' service
  // you might consider moving them to a 'base modal' service for a real-life application
  return {
    get: get,
    setHandler: setHandler
  };

  ////////////////////////////
  // *** Implementation *** //
  ////////////////////////////

  /**
   * Returns a modal instance by id.
   */
  function get(id) {
    return modals.find(function (modal) { return modal.baseModal.id === id; }) ||
      createModal(id);
  }

  /**
   * Attaches a directive handler which allows to manipulate modal state.
   */
  function setHandler(id, handler) {
    get(id).directiveHandler = handler;
  }

  /**
   * Creates a new modal instance.
   */
  function createModal(id) {
    var modal = {
      show: show,
      close: close,
      baseModal: customModal.get(id),
      // activates view with the given name
      activateView: activateView,
      // activates the previous view in hierarchy
      previousView: previousView
    };
    modal.baseModal.callbacks.afterClosed = afterClosed(modal);
    // adds modal to the array with the other modals.
    modals.push(modal);
    return modal;

    /**
     * Triggers the 'open' event.
     */
    function show() {
      this.baseModal.show();
    }

    /**
     * Triggers the 'close' event.
     */
    function close() {
      this.baseModal.close();
    }

    /**
     * Activates view with the given name.
     */
    function activateView(name) {
      this.directiveHandler.activateView(name);
    }

    /**
     * Activates the previous view in hierarchy.
     */
    function previousView() {
      this.directiveHandler.previousView();
    }

    /**
     * Clears inputs and pre-activates the root view if required.
     */
    function afterClosed(modal) {
      var m = modal;
      return function () {
        var handler = m.directiveHandler;
        // `erasable` determines if all input data is erased after the modal is closed
        if (handler.options.erasable) handler.clearInputs();
        // `returnable` determines if the root view should be displayed to the user
        // when the modal is opened for the next time
        if (handler.options.returnable) handler.activateRoot();
      };
    }
  }

}]);
