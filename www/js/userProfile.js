//userprofile.js

'Use Strict';
angular.module('TembaFire').controller('userProfileController', function($scope, $state, $stateParams, $localStorage, Utils, Popup, $timeout, Service, $ionicTabsDelegate, $ionicHistory, Watchers, $ionicModal, $ionicPopup) {

 $scope.$on('$ionicView.enter', function() {


     // Make the parameters accessible on $scope:
     $scope.params = $stateParams;
     console.log('params ', $scope.params);
     console.log('userID '+ $scope.params.userID);


       //GET AND DISPLAY USER PROFILE DATA:
    $scope.userProfile =  Service.getUserProfile($scope.params.userID);
    console.log('userprofile ', $scope.userProfile);
    $scope.userProfilePic=$scope.userProfile['profilePic'];
    console.log('pic', $scope.userProfilePic);
    $scope.userName=$scope.userProfile['username'];
    console.log('userName', $scope.userName);

     $scope.userTags=$scope.userProfile['userTags'];
     $scope.userWebUrl=$scope.userProfile['userWebUrl'];
     console.log('URL' + $scope.userWebUrl);

     $scope.userBio=$scope.userProfile['userBio'];


     //GET CHATS:

    $scope.chats = [];
    $scope.chats = Service.getChatList();
    console.log('getchatlist  ' , $scope.chats);
   $scope.chat=Service.getChatById($localStorage.chatId);

  });

  //Proceed to chat in the selected chatstream:
  $scope.enterChat= function(chat) {
    $localStorage.chatId = chat.id;
      console.log('chatId '+ chat.id);
    $scope.canChangeView = true;
    $state.go('app.chat');
  };

//make ProfilePics clickable to lead to profilepage:
   $scope.goToProfile = function(profile) {


         for (var i = 0; i < $scope.chats.length; i++) {
           console.log('chatCreator' + $scope.chats[i].chatCreator);
       $scope.userInfo=Service.getUserProfile($scope.chats[i].chatCreator);

            $scope.id=$scope.userInfo['id'];
             console.log('userInfo ', $scope.userInfo);

           //$scope.userID=$scope.userID.id;
             console.log('userInfo.id ' + $scope.id);

       $scope.canChangeView = true;
       $state.go('app.userProfile', {userID: $scope.id});
          };

    };


});
