// profile.js
// This is the controller that handles the main view when the user is successfully logged in.
// The account currently logged in can be accessed through localStorage.account.
// The authenticated user can be accessed through firebase.auth().currentUser.
'Use Strict';
angular.module('TembaFire').controller('profileController', function($scope, $state, $stateParams, $localStorage, Utils, Popup, $timeout, Service, $ionicTabsDelegate, $ionicHistory, Watchers, $ionicModal, $ionicPopup) {

 $scope.$on('$ionicView.enter', function() {



    //Notify whenever there are new messages.
    $scope.$watch(function() {
      return Service.getUnreadMessages();
    }, function(unreadMessages) {
      $scope.unreadMessages = unreadMessages;
    });

    //Update profile whenever there are changes done on the profile.
    $scope.$watch(function() {
      return Service.getProfile();
    }, function(profile) {
      if($scope.changedProfilePic)
        Utils.show();
      $scope.profile = Service.getProfile();
        console.log('profile', $scope.profile);
      console.log($scope.profile.username);
    });

    //Update UserBio and website//
    $scope.$watch(function() {
       return Service.getEditProfileData();
    }, function(editProfileData) {
      $scope.editProfileData = Service.getEditProfileData();
      $scope.editProfileData.userBio =  Service.getEditProfileData().userBio;
      $scope.editProfileData.userTags =  Service.getEditProfileData().userTags;
      console.log($scope.editProfileData.userBio);
    });


    //Notify whenever there are new friend requests.
    $scope.$watch(function() {
      return Service.getFriendRequestsCount();
    }, function(friendRequests) {
      $scope.friendRequestsCount = friendRequests;
    });

    //Notify whenever there are new group messages.
    $scope.$watch(function() {
      return Service.getUnreadGroupMessages();
    }, function(unreadGroupMessages) {
      $scope.unreadGroupMessages = unreadGroupMessages;
    });

     //keep user chat list updated:

    $scope.chats = [];
    $scope.chats = Service.getChatList();
     console.log('chatlist' + $scope.chats)


    $scope.changedProfilePic = false;
    //Disable canChangeView to disable automatically restating to messages route whenever Firebase Watcher calls are triggered.
    $scope.canChangeView = false;
    //Select the 4th tab on the footer to highlight the profile icon.
    //$ionicTabsDelegate.select(3);

 });

     /// start MODAL ///

        // Create form data object for the editProfile modal
    $scope.editProfileData = {
        userBio: '',
        userWebUrl: '',
        userTags: [],
    };

    console.log('bio: ' + $scope.editProfileData.userBio + ' web: ' +  $scope.editProfileData.userWebUrl );


    // Create the click-summonable editProfile modal
  $ionicModal.fromTemplateUrl('templates/editProfile.html', {
        scope: $scope //give modal access to parent scope
    }).then(function(modal) {
        $scope.modal = modal;
    });

  // Trigger the editProfile modal to close
  $scope.closeEditProfile = function() {
        $scope.modal.hide();
    };

    // Open the editProfile modal
  $scope.editProfile = function() {
        $scope.modal.show();
        console.log('open modal');
    };

  // Perform the profile-editing action when the user submits the editProfile form
  $scope.doEdits = function(editProfileData) {

        console.log('Editing Profile');

        firebase.database().ref().child('accounts/'+$localStorage.accountId).update({
            userBio: $scope.editProfileData.userBio,
            userWebUrl: $scope.editProfileData.userWebUrl,
            userTags: $scope.editProfileData.userTags

        });

  };

    // popup of edit confirmation
  $scope.infoApp2 = function() {
        var alertPopup = $ionicPopup.alert({
            template: '<center>Profile Updated!</center>',
            buttons: [
                {
                    text: 'Ok',
                    type: 'button-dark'
                }
            ]
        });
  alertPopup.then(function(res) {
            console.log('commit edits!!');
        });
  };

 ///  end MODAL  //////


  //Set profile image while deleting the previous uploaded profilePic.

  $scope.$on('imageUploaded', function(event, args) {
    $scope.changedProfilePic = true;
    firebase.database().ref('accounts/' + $localStorage.accountId).once('value', function(account) {
      if(account.val().profilePic != 'img/profile.png')
        firebase.storage().refFromURL(account.val().profilePic).delete();

    firebase.database().ref('accounts/' + $localStorage.accountId).update({
      profilePic: args.imageUrl
    });
    for (var i = 0; i < $scope.chats.length; i++) {
        var chat = $scope.chats[i];
        var chatCreator = $scope.chats[i].chatCreator;
        if (chatCreator = $localstorage.accountId)
            firebase.database().ref('chats/' + chat ).update({
      chatCreatorProfilePic: args.imageUrl

        });
     };
    });
  });

  //Logout the user. Clears the localStorage as well as reinitializing the variable of watcherAttached to only trigger attaching of watcher once.
  $scope.logout = function() {
    if (firebase.auth()) {
      firebase.database().ref('accounts/' + $localStorage.accountId).update({
        online: false
      });
      firebase.auth().signOut().then(function() {
        Watchers.removeWatchers();
        //Clear the saved credentials.
        $localStorage.$reset();
        //Proceed to login screen.
        $scope.canChangeView = true;
        $state.go('login');
      }, function(error) {
        //Show error message.
        Utils.message(Popup.errorIcon, Popup.errorLogout);
      });
    } else {
      //Clear the saved credentials.
      $localStorage.$reset();
      //Proceed to login screen.
      $scope.canChangeView = true;
      $state.go('login');
    }
  };

  //Function to assign a profile picture, calls imageUploaded function on top when Firebase is done uploading the image.
  $scope.changeProfilePic = function() {
    var popup = Utils.confirm('ion-link', 'Profile Picture: Do you want to take a photo or choose from your gallery?', 'ion-images', 'ion-camera');
    popup.then(function(isCamera) {
      var imageSource;
      if (isCamera) {
        imageSource = Camera.PictureSourceType.CAMERA;
      } else {
        imageSource = Camera.PictureSourceType.PHOTOLIBRARY;
      }
      //Show loading.
      Utils.getProfilePicture(imageSource);
    });
  };

  //Constrains our selected picture to be of same width and height, to preserve proportion.
  $scope.constrainProportion = function() {
    if($scope.changedProfilePic) {
      Utils.hide();
      $scope.changedProfilePic = false;
    }
    var img = document.getElementById('profilePic');
    var width = img.width;
    img.style.height = width + "px";
  };


  //Proceed to chat in the selected chatstream:
  $scope.enterChat= function(chat) {
    $localStorage.chatId = chat.id;
      console.log('chatId '+ chat.id);
    $scope.canChangeView = true;
    $state.go('app.chat');
  };

//make ProfilePics clickable to lead to profilepage:
   $scope.goToProfile = function(profile) {


         for (var i = 0; i < $scope.chats.length; i++) {
           console.log('chatCreator' + $scope.chats[i].chatCreator);
       $scope.userInfo=Service.getUserProfile($scope.chats[i].chatCreator);

            $scope.id=$scope.userInfo['id'];
            console.log('userInfo', $scope.userInfo);

             console.log('userInfo.id ' + $scope.id);

       $scope.canChangeView = true;
       $state.go('app.userProfile', {userID: $scope.id});

            };

    };

});
