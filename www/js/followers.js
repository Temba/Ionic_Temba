// followers.js
// This is the controller that handles the users's follower list: those users _followed by_ the focal user, and the users _following_ the focal user.
//This view allows users to interact with follower  profiles and find users to follow.

'Use Strict';
angular.module('TembaFire').controller('followersController', function($scope, $state, $localStorage, Popup, $timeout, Utils, Watchers, Service, $ionicTabsDelegate, $ionicHistory) {




    //The following is as normal from Firebase Messenger

  $scope.$on('$ionicView.enter', function() {
    $scope.mode = 'Followers';
    //Initialize our models from the Service.
    $scope.followers = [];
    //$scope.followers = Service.getFollowerList();


    //Disable canChangeView to disable automatically restating to messages route whenever Firebase Watcher calls are triggered.
    $scope.canChangeView = false;

  });




});
