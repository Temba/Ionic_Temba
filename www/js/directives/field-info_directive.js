(function() {

  'use strict';

  angular.module('fieldInfoIcon', [])

 .directive('fieldInfoIcon', fieldInfoIcon);

  /**
   * @ngInject
   */
  function fieldInfoIcon() {

    return {

      restrict: 'C',

      link: function(scope, element, attrs) {

        element.on('click', function() {
          console.log('Show info message for ' + attrs.infoTopic);
        });

      }

    };

  }

})();
