angular.module('multiViewModal.directives', [])
//Modified from  Evgenij Olshanskij 2016

.directive('multiViewModal', ['multiViewModal', '$compile',
  function (multiViewModal, $compile) {

  return {
    restrict: 'E',
    link: link
  };

  ////////////////////////////
  // *** Implementation *** //
  ////////////////////////////

  function link(scope, element, attrs) {

    var id = attrs.id;
    var views = 'view_' + id;

    // reads views defined as child directive elements
    scope[views] = readViews(element.children());
    // sets 'isActive' flag for the root view
    scope[views].find(function(view) { return view.root; }).isActive = true;

    // reads options defined as directive attributes
    var options = {
      // controls if all data should be erased after a modal is closed
      erasable:   attrs.erasable ? attrs.erasable === 'true' : true,
      // controls if the current active view will remain active when the modal reopened
      returnable: attrs.returnable ? attrs.returnable === 'true' : true
    };

    // initializes and updates directive template
    var viewsTemplate = '<ion-pane ng-repeat="item in ' + views + '"' +
      ' ng-show="item.isActive" ng-include="item.url"></ion-pane>';

    var baseModalTemplate = '<custom-modal id="' + id + '">' +
        viewsTemplate +
      '</custom-modal>';

    var baseModal = $compile(baseModalTemplate)(scope);
    element.replaceWith(baseModal);

    // registers directive handler in the multiViewModal service
    multiViewModal.setHandler(id, handler());

    /**
     * Retrieves an information about the views
     * from the directive child elements and their attrs.
     */
    function readViews(childElements) {
      return Array.from(childElements).reduce(function (views, viewItem) {
        if (viewItem.localName === 'view-item') {
          views.push(
            ['name', 'url', 'root', 'parent'].reduce(function(view, attrName) {
              var attribute = viewItem.attributes[attrName];
              if (attribute) {
                var value = attribute.value;
                view[attrName] =
                  value === 'true' ||
                  value === 'false' ? value === 'true' : value;
              }
              return view;
            }, {})
          );
        }
        return views;
      }, []);
    }

    /**
     * Creates a handler to manipulate the directive state.
     */
    function handler() {
      return {
        options: options,
        activateRoot: activateRoot, // activates the root view
        activateView: activateView, // activates view with the given name
        previousView: previousView, // activates the previous view in hierarchy
        clearInputs:  clearInputs   // clears all inputs by recompiling the modal
      };

      // sets view with an appropriate name as active.
      function activateView(name) {
        scope[views].forEach(function (view) {
          if (view.name === name)
            view.isActive = true;
          else
            delete view.isActive;
        });
      }

      // activates the root view
      function activateRoot() {
        activateView(scope[views]
          .find(function (view) { return view.root; }).name);
      }

      // goes back to the previous view.
      function previousView() {
        activateView(scope[views]
          .find(function (view) { return view.isActive; })
          .parent);
      }

      // clears inputs by recompiling the modal
      function clearInputs() {
        var pane = angular.element(baseModal.children()[0]);
        pane.empty();
        pane.append($compile(viewsTemplate)(scope));
      }
    }
  }
}]);
