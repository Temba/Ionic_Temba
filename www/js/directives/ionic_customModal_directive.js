angular.module('customModal.directives', [])
/**
* Renders modal template.

Modified from  Evgenij Olshanskij 2016
*/
.directive('customModal', ['customModal', '$compile',
  function (customModal, $compile) {

  return {
    restrict: 'E',
    transclude: true,
    link: link
  };

  ////////////////////////////
  // *** Implementation *** //
  ////////////////////////////

  function link(scope, element, attrs, ctrl, transclude) {

    // gets the directive id
    var id = attrs.id;
    // creates a unique variable name for `ng-hide`
    var ngHideBinder = "hidden_" + id;
    var modalEl = '<ion-pane ng-hide="' +
      ngHideBinder +
      '" class="menu-animation ng-hide"></ion-pane>';
    scope[ngHideBinder] = true;
    // gets the directive content and appends it to a modal element
    transclude(function(clone) {
      var wrapper = $compile(modalEl)(scope);
      element.append(wrapper.append(clone));
    });
    // registers directive handler in the customModal service
    customModal.setHandler(id, handler());

    //////////////////////////////////

    // object that provides methods for the modal visibility handling
    function handler() {
      return {
        show: show,
        close: close,
        isHidden: isHidden
      };

      // shows the modal
      function show() {
        scope[ngHideBinder] = false;
      }

      // hides the modal
      function close() {
        scope[ngHideBinder] = true;
      }

      // checks if the modal is hidden / visible
      function isHidden() {
        return scope[ngHideBinder];
      }
    }

  }

}]);
