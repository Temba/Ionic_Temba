// chat.js
// This is the controller that handles the chat messages for a chatstream.
'Use Strict';
angular.module('TembaFire').controller('chatController', function($scope, $state, $stateParams, Giphy, $localStorage, Popup, Utils, $filter, $ionicScrollDelegate, $ionicHistory, Service, $timeout, $cordovaCamera) {
     //UNCOMMENTED FEB
    //Prevent automatically restarting to messages route when Firebase Watcher calls are triggered.
  $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
    if (!$scope.canChangeView) {
      event.preventDefault();
    }
  });
//

  //Allow going back when back is selected.
  $scope.back = function() {
    $scope.canChangeView = true;
    $localStorage.chatId = undefined;
    $ionicHistory.goBack();
    //$state.go('app.chats');
  };

// ADDED FEB
  //Allow changing to other views when tab is selected.
  $scope.changeTab = function(stateTo) {
    $ionicHistory.nextViewOptions({
      disableAnimate: true
    });
    $scope.canChangeView = true;
    $state.go(stateTo);
  };

//

  $scope.$on('$ionicView.enter', function() {
    //Disable scroll to correctly orient the keyboard input for iOS.
    //***COMMENTED OUT TO PREVIEW IN WEB; RE-ENABLE IN PRODUCTION (FEB 2017) ***
   // cordova.plugins.Keyboard.disableScroll(true);

    //Set scope variables to the selected chat.
      console.log($localStorage.chatId);
    if ($localStorage.chatId) {

      var chat = Service.getChatById($localStorage.chatId);
      $scope.chatName = chat.name;
      console.log(chat);
     //$scope.chatTags =  chat.chatTags;
        //console.log ('tags' + chat.chatTags);
      $scope.chatmessages = chat.chatmessages;
      $scope.unreadChatmessages = chat.unreadChatmessages;
       console.log($scope.chatmessages);
      for (var i = 0; i < $scope.chatmessages.length; i++) {
        $scope.chatmessages[i].profilePic = Service.getProfilePic($scope.chatmessages[i].sender);
      }
      $scope.scrollBottom();
      if ($localStorage.chatId) {
        //Update users read chatmessages on Firebase.
        $scope.chatId = $localStorage.chatId;
        $scope.updateChatmessagesRead();
        console.log('scroll and update' + $scope.chatId);
       }
    }
    //Disable canChangeView to disable automatically restating to messages route whenever Firebase Watcher calls are triggered.
    $scope.canChangeView = false;
  });

  //Broadcast from our Watcher that tells us that a new message has been added to the chat.
  $scope.$on('chatmessageAdded', function() {
    //Scroll to bottom and updateChatmessagesRead, as well as setting the lastchatActiveDate.
    $scope.scrollBottom();
    $scope.updateChatmessagesRead();
    console.log('on chatmessage added');
    $timeout(function () {
      Service.setChatLastActiveDate($localStorage.chatId, new Date());
    });
  });

  //Broadcast from our Utils.getPicture function that tells us that the image selected has been uploaded.
  $scope.$on('imageUploaded', function(event, args) {
    //Proceed with sending of image message.
    $scope.sendChatmessage('image', args.imageUrl);
  });

  //Send picture message, ask if the image source is gallery or camera.
  $scope.sendPictureMessage = function() {
    var popup = Utils.confirm('ion-link', 'Photo Message: Do you want to take a photo or choose from your gallery?', 'ion-images', 'ion-camera');
    popup.then(function(isCamera) {
      var imageSource;
      if (isCamera) {
        imageSource = Camera.PictureSourceType.CAMERA;
      } else {
        imageSource = Camera.PictureSourceType.PHOTOLIBRARY;
      }
      //Show loading.
      Utils.getPicture(imageSource);
    });
  };

  //Send text message.
  $scope.sendTextMessage = function() {
    if ($scope.chatmessage != '') {
        console.log('chatmessage is ' + $scope.chatmessage);
      $scope.sendChatmessage('text', $scope.chatmessage);

    }
  };

  //Scroll to bottom so new messages will be seen.
  $scope.scrollBottom = function() {
    $ionicScrollDelegate.scrollBottom(true);
  };

  //Scroll to top.
  $scope.scrollTop = function() {
    $ionicScrollDelegate.scrollTop(true);
  };

  //Send message, create Firebase data.
  $scope.sendChatmessage = function(type, chatmessage) {
    if ($scope.chatId) {
      //Has existing conversation
      firebase.database().ref('chats/' + $scope.chatId).once('value', function(chat) {
        var chatmessages = chat.val().chatmessages;
        if (!chatmessages) {
          chatmessages = [];
        }
        if (type == 'text') {
          chatmessages.push({
            sender: $localStorage.accountId,
            chatmessage: chatmessage,
            date: Date(),
            type: 'text'
            });
        } else {
          chatmessages.push({
            sender: $localStorage.accountId,
            image: chatmessage,
            date: Date(),
            type: 'image'
          });
        }
        firebase.database().ref('chats/' + $scope.chatId).update({
          chatmessages: chatmessages
        });
           console.log('sendchat');
      });
    }

    //Clear, and refresh to see the new chatmessages.
    $scope.chatmessage = '';
    $scope.scrollBottom();
    console.log ('clear and refresh');
  };

  //Enlarge selected image when selected on view.
  $scope.enlargeImage = function(url) {
    Utils.image(url);
  };

  //Update users' chatmessagesRead on Firebase database.
  $scope.updateChatmessagesRead = function() {
    firebase.database().ref('accounts/' + $localStorage.accountId).once('value', function(account) {
      var chats = account.val().chats;
      angular.forEach(chats, function(chat) {
        if (chat.chat == $scope.chatId) {
          chat.chatmessagesRead = $scope.chatmessages.length;
            console.log('updating account>chatmessages read' + $scope.chatmessages.length);
        };

      });
      firebase.database().ref('accounts/' + $localStorage.accountId).update({
        chats: chats
      });
    });
      console.log('updating accounts>account>chats');
  };

  //Open chat details, where user can add participants or leave the chat.
  $scope.details = function() {
    $scope.canChangeView = true;
    $state.go('app.chatDetails');
  };

 //giphy integration
  $scope.gifs = [];
  $scope.gifQuery = '';
  $scope.isGifShown = false;
  $scope.isGifLoading = false;

    //Broadcast from our Utils.getPicture function that tells us that the image selected has been uploaded.

  $scope.$on('imageUploaded', function(event, args) {

    //Proceed with sending of image message.
    $scope.sendGif('image', imageUrl);
  });


    $scope.sendGif = function(imageUrl) {
      console.log(imageUrl);

      $scope.sendChatmessage('image', imageUrl);
          console.log('sendchatmessage ' + imageUrl);
    };

    $scope.openGiphy = function() {
      $scope.isGifShown = true;
      $scope.chatmessage = '';
    };

    $scope.$watch('gifQuery', function(newValue) {
      if (newValue.length) {
        $scope.isGifLoading = true;
        $scope.gifs = [];

        Giphy.search(newValue)
          .then(function(gifs) {
            $scope.gifs = gifs;
            $scope.isGifLoading = false;
          });
      } else {
        _initGiphy();
      }
    });

    // Onload
    var _initGiphy = function() {
      Giphy.trending()
        .then(function(gifs) {
          $scope.gifs = gifs;
        });
    };
    _initGiphy();

    // end giphy integration //

});
